package com.tieto.example.medclinic.util;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

public class ClasspathURLStreamHandler extends URLStreamHandler {
	private final ClassLoader classLoader;

	public ClasspathURLStreamHandler() {
		this.classLoader = getClass().getClassLoader();
	}

	public ClasspathURLStreamHandler(ClassLoader classLoader) {
		this.classLoader = classLoader;
	}

	@Override
	protected URLConnection openConnection(URL url) throws IOException {
		final URL resourceUrl = classLoader.getResource(url.getPath());
		if (resourceUrl == null) {
			throw new IOException("Could not find resource: " + url.getPath());
		}
		return resourceUrl.openConnection();
	}

	public static URLConnection getURLConnection(String url) throws IOException {
		if (url == null || url.isEmpty())
			throw new IOException("Path is null or empty");
		if (url.startsWith("classpath:"))
			return new URL(null, url, new ClasspathURLStreamHandler(Thread.currentThread().getContextClassLoader())).openConnection();

		return new URL(url).openConnection();
	}
}

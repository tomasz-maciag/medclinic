package com.tieto.example.medclinic.exception;

public class DoctorNotFoundException extends Exception {

	private static final long serialVersionUID = 824299574177177710L;

	public DoctorNotFoundException(int id) {
		super("Nie znaleziono lekarza o ID: " + id);
	}
}

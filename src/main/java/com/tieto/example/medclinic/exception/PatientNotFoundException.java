package com.tieto.example.medclinic.exception;

public class PatientNotFoundException extends Exception {

	private static final long serialVersionUID = -4457017542300552634L;

	public PatientNotFoundException(int id) {
		super("Nie znaleziono pacjenta o ID: " + id);
	}
}

package com.tieto.example.medclinic.persistence.dao;

import java.util.Collection;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.tieto.example.medclinic.exception.DoctorNotFoundException;
import com.tieto.example.medclinic.model.DoctorModel;
import com.tieto.example.medclinic.persistence.DBAccessUtils;
import com.tieto.example.medclinic.persistence.dto.DoctorDTO;

public class DoctorDAO extends AbstractDAO {

	public DoctorDAO(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public int save(DoctorModel model) {
		Transaction tx = null;
		try (final Session session = grabOpenSession()) {
			tx = session.beginTransaction();
			DoctorDTO dto = new DoctorDTO(model);
			session.persist(dto);
			session.flush();
			tx.commit();
			return dto.getId();
		} catch (HibernateException e) {
			System.err.println("Error while saving Doctor " + model.getName() + " " + model.getSurname() + ": " + e.toString());
			if (tx != null) {
				tx.rollback();
			}
			throw e;
		}
	}

	public void delete(int doctorID) throws DoctorNotFoundException {
		Transaction tx = null;
		try (final Session session = grabOpenSession()) {
			tx = session.beginTransaction();
			DoctorDTO dto = DBAccessUtils.read(session, doctorID, DoctorDTO.class);
			if (dto == null) {
				tx.rollback();
				throw new DoctorNotFoundException(doctorID);
			}
			session.delete(dto);
			session.flush();
			tx.commit();
		} catch (HibernateException e) {
			System.err.println("Error while deleting doctorID = " + doctorID + ": " + e.toString());
			if (tx != null) {
				tx.rollback();
			}
			throw e;
		}
	}

	public Collection<DoctorDTO> loadAll() {
		List<DoctorDTO> result;
		Transaction tx = null;
		try (final Session session = grabOpenSession()) {
			tx = session.beginTransaction();
			result = DBAccessUtils.selectAll(session, DoctorDTO.class);
			session.flush();
			tx.commit();
		} catch (HibernateException e) {
			System.err.println("Error while loading all Doctors: " + e.toString());
			if (tx != null) {
				tx.rollback();
			}
			throw e;
		}

		return result;
	}

	public DoctorDTO load(int doctorID) throws DoctorNotFoundException {
		DoctorDTO result = null;
		Transaction tx = null;
		try (final Session session = grabOpenSession()) {
			tx = session.beginTransaction();
			result = DBAccessUtils.read(session, doctorID, DoctorDTO.class);
			if (result == null) {
				tx.rollback();
				throw new DoctorNotFoundException(doctorID);
			}
			session.flush();
			tx.commit();
		} catch (HibernateException e) {
			System.err.println("Error while loading doctorID = " + doctorID + ": " + e.toString());
			if (tx != null) {
				tx.rollback();
			}
			throw e;
		}

		return result;
	}

	public long count() {
		long result = 0;
		Transaction tx = null;
		try (final Session session = grabOpenSession()) {
			tx = session.beginTransaction();
			result = DBAccessUtils.count(session, DoctorDTO.class);
			tx.commit();
		} catch (HibernateException e) {
			System.err.println("Error while counting rows of " + DoctorDTO.class.getSimpleName() + " " + e.toString());
			if (tx != null) {
				tx.rollback();
			}
			throw e;
		}
		return result;
	}
}

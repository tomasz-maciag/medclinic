package com.tieto.example.medclinic.persistence;

import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class HbnSession {

	protected SessionFactory factory;

	public static class HbnSessionHolder {
		static HbnSession INSTANCE = new HbnSession();
	}

	public static HbnSession getInstance() {
		return HbnSessionHolder.INSTANCE;
	}

	public SessionFactory get() {
		return factory;
	}

	public HbnSession fromConfig(final String hbnCfgXmlFileName) {
		if (factory == null) {
			StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure(hbnCfgXmlFileName).build();
			factory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
			factory.getStatistics().setStatisticsEnabled(true);
		}
		return this;
	}
}

package com.tieto.example.medclinic.persistence.dao;

import java.util.Collection;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.tieto.example.medclinic.exception.PatientNotFoundException;
import com.tieto.example.medclinic.model.PatientModel;
import com.tieto.example.medclinic.persistence.DBAccessUtils;
import com.tieto.example.medclinic.persistence.dto.DoctorDTO;
import com.tieto.example.medclinic.persistence.dto.PatientDTO;

public class PatientDAO extends AbstractDAO {

	public PatientDAO(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public int save(PatientModel model, DoctorDTO doctorDto) {
		Transaction tx = null;
		try (final Session session = grabOpenSession()) {
			tx = session.beginTransaction();
			PatientDTO dto = new PatientDTO(model, doctorDto);
			session.persist(dto);
			session.flush();
			tx.commit();
			return dto.getId();
		} catch (HibernateException e) {
			System.err.println("Error while saving Patient " + model.getName() + " " + model.getSurname() + ": " + e.toString());
			if (tx != null) {
				tx.rollback();
			}
			throw e;
		}
	}

	public void delete(int patientID) throws PatientNotFoundException {
		Transaction tx = null;
		try (final Session session = grabOpenSession()) {
			tx = session.beginTransaction();
			PatientDTO dto = DBAccessUtils.read(session, patientID, PatientDTO.class);
			if (dto == null) {
				tx.rollback();
				throw new PatientNotFoundException(patientID);
			}
			dto.getDoctor().deletePatient(patientID);
			session.delete(dto);
			session.flush();
			tx.commit();
		} catch (HibernateException e) {
			System.err.println("Error while deleting patientID = " + patientID + ": " + e.toString());
			if (tx != null) {
				tx.rollback();
			}
			throw e;
		}
	}

	public Collection<PatientDTO> loadAll() {
		List<PatientDTO> result;
		Transaction tx = null;
		try (final Session session = grabOpenSession()) {
			tx = session.beginTransaction();
			result = DBAccessUtils.selectAll(session, PatientDTO.class);
			session.flush();
			tx.commit();
		} catch (HibernateException e) {
			System.err.println("Error while loading all Patients: " + e.toString());
			if (tx != null) {
				tx.rollback();
			}
			throw e;
		}

		return result;
	}

	public PatientDTO load(int patientID) throws PatientNotFoundException {
		PatientDTO result = null;
		Transaction tx = null;
		try (final Session session = grabOpenSession()) {
			tx = session.beginTransaction();
			result = DBAccessUtils.read(session, patientID, PatientDTO.class);
			if (result == null) {
				tx.rollback();
				throw new PatientNotFoundException(patientID);
			}
			session.flush();
			tx.commit();
		} catch (HibernateException e) {
			System.err.println("Error while loading patientID = " + patientID + ": " + e.toString());
			if (tx != null) {
				tx.rollback();
			}
			throw e;
		}

		return result;
	}

	public long count() {
		long result = 0;
		Transaction tx = null;
		try (final Session session = grabOpenSession()) {
			tx = session.beginTransaction();
			result = DBAccessUtils.count(session, PatientDTO.class);
			tx.commit();
		} catch (HibernateException e) {
			System.err.println("Error while counting rows of " + PatientDTO.class.getSimpleName() + " " + e.toString());
			if (tx != null) {
				tx.rollback();
			}
			throw e;
		}
		return result;
	}
}

package com.tieto.example.medclinic.persistence.migration;

public class DBConnectionInfo {

	private final String connectionUrl;
	private final String username;
	private final String password;
	private final String driver;
	private final String hostAndPort, databaseName;

	public DBConnectionInfo(final String connectionUrl, final String username, final String password, final String driver) {
		this.connectionUrl = connectionUrl;
		this.username = username;
		this.password = password;
		this.driver = driver;

		int slash = connectionUrl.lastIndexOf('/');
		hostAndPort = connectionUrl.substring(0, slash + 1);
		databaseName = connectionUrl.substring(slash + 1, connectionUrl.length()).toLowerCase();

		if (!isValid())
			throw new RuntimeException("Can't read database connection properties");
	}

	public String getConnectionUrl() {
		return connectionUrl;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getDriver() {
		return driver;
	}

	public String getHostAndPort() {
		return hostAndPort;
	}

	public String getDatabaseName() {
		return databaseName;
	}

	public boolean isValid() {
		return (connectionUrl != null && !connectionUrl.isEmpty()) &&
				(username != null && !username.isEmpty()) &&
				(password != null && !password.isEmpty()) &&
				(driver != null && !driver.isEmpty());
	}
}

package com.tieto.example.medclinic.persistence.migration;

import java.io.IOException;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.flywaydb.core.Flyway;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.tieto.example.medclinic.util.ClasspathURLStreamHandler;

public class DBMigration {

	public static final String[] LOCATIONS = { "db/flyway", "com.tieto.example.medclinic.persistence.migration" };	// directory AND package name
	private static final String DEFAULT_DB = "postgres";

	public static void init(String classpathCfg) {
		migrateDatabase(createDatabaseIfNotExist(classpathCfg));
	}

	private static void migrateDatabase(final DBConnectionInfo dbInfo) {
		Flyway flyway = new Flyway();
		flyway.setDataSource(dbInfo.getConnectionUrl(), dbInfo.getUsername(), dbInfo.getPassword());
		flyway.setLocations(LOCATIONS);
		flyway.migrate();
	}

	private static DBConnectionInfo createDatabaseIfNotExist(String classpathCfg) throws RuntimeException {
		DBConnectionInfo dbInfo = null;
		try {
			dbInfo = readHibernateXML("classpath:" + classpathCfg);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			System.err.println(e.toString());
			throw new RuntimeException(e);	// can't continue in case of such errors
		}

		try {
			Class.forName(dbInfo.getDriver());
		} catch (ClassNotFoundException e) {
			System.err.println(e.toString());
			throw new RuntimeException(e);	// can't continue in case of such errors
		}

		String postgresDefaultUrl = dbInfo.getHostAndPort() + DEFAULT_DB;
		System.out.println("Connecting to " + postgresDefaultUrl);
		try (Connection connection = DriverManager.getConnection(postgresDefaultUrl, dbInfo.getUsername(), dbInfo.getPassword())) {

			try (PreparedStatement databaseExists = connection.prepareStatement("SELECT 1 FROM pg_database WHERE datname = ?")) {
				databaseExists.setString(1, dbInfo.getDatabaseName());

				try (ResultSet rs = databaseExists.executeQuery()) {
					if (!rs.next()) {
						System.out.println("Creating new database named: " + dbInfo.getDatabaseName());
						try (Statement createDatabase = connection.createStatement()) {
							boolean transactionsEnabled = !connection.getAutoCommit();
							if (transactionsEnabled)
								connection.setAutoCommit(true);
							createDatabase.executeUpdate("CREATE DATABASE \"" + dbInfo.getDatabaseName() + "\"");
							if (transactionsEnabled)
								connection.setAutoCommit(false);
						}
					} else {
						System.out.println("Database " + dbInfo.getDatabaseName() + " already exists");
					}
				}
			}
		} catch (SQLException e) {
			String sqlState = e.getSQLState();
			if (sqlState.equals("42P04"))		// see Postgres error codes
				System.out.println("Database " + dbInfo.getDatabaseName() + " already exists");
			else {
				System.err.println(e.toString() + ", SQLState: " + sqlState + ", ErrorCode: " + e.getErrorCode());
				throw new RuntimeException(e);	// can't continue in case of such errors
			}
		}

		return dbInfo;
	}

	private static DBConnectionInfo readHibernateXML(final String classpathfileName) throws ParserConfigurationException, SAXException, IOException {
		URLConnection file = ClasspathURLStreamHandler.getURLConnection(classpathfileName);
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
		Document doc = docBuilder.parse(file.getInputStream());
		Element root = doc.getDocumentElement();
		root.normalize();

		DBConnectionInfo info = new DBConnectionInfo(
			findNode(root.getChildNodes(), "connection.url"),
			findNode(root.getChildNodes(), "connection.username"),
			findNode(root.getChildNodes(), "connection.password"),
			findNode(root.getChildNodes(), "connection.driver_class")
		);

		return info;
	}

	private static String findNode(final NodeList nodeList, final String searchedName) {
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node tempNode = nodeList.item(i);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
				if (tempNode.hasAttributes()) {
					NamedNodeMap nodeMap = tempNode.getAttributes();
					for (int j = 0; j < nodeMap.getLength(); ++j) {
						Node node = nodeMap.item(j);
						if (node.getNodeValue().equals(searchedName))
							return tempNode.getTextContent();
					}
				}
				if (tempNode.hasChildNodes()) {
					String result = findNode(tempNode.getChildNodes(), searchedName);
					if (result != null)
						return result;
				}
			}
		}
		return null;
	}

}

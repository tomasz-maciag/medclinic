package com.tieto.example.medclinic.persistence;

import java.io.Serializable;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.query.Query;

public class DBAccessUtils {

	@SuppressWarnings("unchecked")
	public static <T> T read(final Session session, final Serializable id, Class<?> clazz) {
		return (T)session.get(clazz, id, LockMode.PESSIMISTIC_WRITE);
	}

	public static <T> int deleteAll(final Session session, Class<T> clazz) {
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaDelete<T> cdel = cb.createCriteriaDelete(clazz);
		cdel.from(clazz);
		return session.createQuery(cdel).executeUpdate();
	}

	public static <T> List<T> selectAll(final Session session, Class<T> clazz) {
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<T> criteriaQuery = builder.createQuery(clazz);
		Root<T> root = criteriaQuery.from(clazz);
		criteriaQuery.select(root);
		Query<T> query = session.createQuery(criteriaQuery);
		return query.getResultList();
	}

	public static <T, V> List<T> selectWhere(final Session session, String column, V value, Class<T> clazz, String compositeKey) {
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<T> criteriaQuery = cb.createQuery(clazz);
		Root<T> root = criteriaQuery.from(clazz);
		if (compositeKey != null && !compositeKey.isEmpty()) {
			criteriaQuery.select(root).where(cb.equal(root.get(compositeKey).get(column), value));
		} else {
			criteriaQuery.select(root).where(cb.equal(root.get(column), value));
		}
		Query<T> query = session.createQuery(criteriaQuery);
		return query.getResultList();
	}

	public static long count(Session session, Class<?> clazz) {
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Long> query = cb.createQuery(Long.class);
		query.select(cb.count(query.from(clazz)));
		return session.createQuery(query).getSingleResult();
	}
}

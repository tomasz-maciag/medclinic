package com.tieto.example.medclinic.persistence.dto;

import java.util.Collection;
import java.util.LinkedList;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.tieto.example.medclinic.model.DoctorModel;

@Entity
@Table(name = "doctor")
public class DoctorDTO {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DOCTOR_ID_GEN")
	@SequenceGenerator(name="DOCTOR_ID_GEN", sequenceName="doctor_id_seq", allocationSize=1)
	private int id;

	private String name;
	private String surname;
	private String title;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "doctor", cascade=CascadeType.ALL)
	private Collection<PatientDTO> patients;

	public DoctorDTO() {
	}

	public DoctorDTO(DoctorModel model) {
		name = model.getName();
		surname = model.getSurname();
		title = model.getTitle();

		if (patients == null)
			patients = new LinkedList<>();

		if (model.getPatients() != null)
			model.getPatients().forEach(p -> patients.add(new PatientDTO(p, this)));
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public String getTitle() {
		return title;
	}

	public int countPatients() {
		return patients.size();
	}

	public void deletePatient(int patientID) {
		patients.removeIf(p -> p.getId() == patientID);
	}
}

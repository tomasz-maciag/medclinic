package com.tieto.example.medclinic.persistence.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class AbstractDAO {

	private SessionFactory sessionFactory;

	protected AbstractDAO(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	protected Session grabOpenSession() {
		return sessionFactory.openSession();
	}
}

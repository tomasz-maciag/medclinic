package com.tieto.example.medclinic.persistence.dto;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.tieto.example.medclinic.model.PatientModel;

@Entity
@Table(name = "patient")
public class PatientDTO {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PATIENT_ID_GEN")
	@SequenceGenerator(name="PATIENT_ID_GEN", sequenceName="patient_id_seq", allocationSize=1)
	private int id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "doctor_id")
	private DoctorDTO doctor;
	private String name;
	private String surname;

	public PatientDTO() {
	}

	public PatientDTO(PatientModel model, DoctorDTO doctorDto) {
		name = model.getName();
		surname = model.getSurname();
		doctor = doctorDto;
	}

	public int getId() {
		return id;
	}

	public DoctorDTO getDoctor() {
		return doctor;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}
}

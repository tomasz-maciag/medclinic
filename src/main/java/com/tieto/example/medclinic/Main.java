package com.tieto.example.medclinic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLConnection;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tieto.example.medclinic.exception.DoctorNotFoundException;
import com.tieto.example.medclinic.exception.PatientNotFoundException;
import com.tieto.example.medclinic.logic.Subject;
import com.tieto.example.medclinic.model.DoctorModel;
import com.tieto.example.medclinic.model.DoctorsModel;
import com.tieto.example.medclinic.model.PatientModel;
import com.tieto.example.medclinic.persistence.HbnSession;
import com.tieto.example.medclinic.persistence.dao.DoctorDAO;
import com.tieto.example.medclinic.persistence.dao.PatientDAO;
import com.tieto.example.medclinic.persistence.dto.DoctorDTO;
import com.tieto.example.medclinic.persistence.dto.PatientDTO;
import com.tieto.example.medclinic.persistence.migration.DBMigration;
import com.tieto.example.medclinic.ui.UiController;
import com.tieto.example.medclinic.ui.UiListener;
import com.tieto.example.medclinic.util.ClasspathURLStreamHandler;

public class Main {

	private static final String HIBERNATE_CONFIG = "db/hibernate.cfg.xml";

	public static void main(String[] args) throws Exception {
		DBMigration.init(HIBERNATE_CONFIG);

		HbnSession session = HbnSession.getInstance().fromConfig(HIBERNATE_CONFIG);
		DoctorDAO doctorDao = new DoctorDAO(session.get());
		PatientDAO patientDao = new PatientDAO(session.get());

		loadAndStoreDefaultModels("model/default.json", session);

		final UiController ui = new UiController(new UiListener() {
			@Override
			public void onUiRead(Subject subject) {
				if (subject == Subject.DOCTOR) {
					for (DoctorDTO dto : doctorDao.loadAll()) {
						System.out.println(String.format(" %4d | %4.4s | %16.16s | %16.16s | pacjenci: %d", dto.getId(), dto.getTitle(), dto.getName(), dto.getSurname(), dto.countPatients()));
					}
				}
				if (subject == Subject.PATIENT) {
					for (PatientDTO dto : patientDao.loadAll()) {
						System.out.println(String.format(" %4d | %16.16s | %16.16s | lekarz: %s %s %s", dto.getId(), dto.getName(), dto.getSurname(), dto.getDoctor().getTitle(), dto.getDoctor().getName(), dto.getDoctor().getSurname()));
					}
				}
			}

			@Override
			public void onUiDelete(Subject subject) {
				if (subject == Subject.DOCTOR) {
					int doctorID = UiController.askForInteger("Podaj ID lekarza: ");
					try {
						doctorDao.delete(doctorID);
						System.out.println("Usunięto doctorID = " + doctorID);
					} catch (DoctorNotFoundException e) {
						System.out.println(e.getMessage());
					}
				}
				if (subject == Subject.PATIENT) {
					int patientID = UiController.askForInteger("Podaj ID pacjenta: ");
					try {
						patientDao.delete(patientID);
						System.out.println("Usunięto patientID = " + patientID);
					} catch (PatientNotFoundException e) {
						System.out.println(e.getMessage());
					}
				}
			}

			@Override
			public void onUiCreate(Subject subject) {
				if (subject == Subject.DOCTOR) {
					DoctorModel model = new DoctorModel()
						.setTitle(UiController.askForString("tytuł"))
						.setName(UiController.askForString("imię"))
						.setSurname(UiController.askForString("nazwisko"));
					int docID = doctorDao.save(model);
					System.out.println("Dodano nowego lekarza z doctorID = " + docID);
				}
				if (subject == Subject.PATIENT) {
					PatientModel model = new PatientModel()
						.setName(UiController.askForString("imię"))
						.setSurname(UiController.askForString("nazwisko"));

					int doctorID = UiController.askForInteger("Podaj ID lekarza do którego należy pacjent: ");
					try {
						DoctorDTO dto = doctorDao.load(doctorID);
						patientDao.save(model, dto);
						System.out.println("Dodano nowego pacjenta do lekarza: " + dto.getTitle() + " " + dto.getName() + " " + dto.getSurname());
					} catch (DoctorNotFoundException e) {
						System.out.println(e.getMessage());
					}
				}
			}
		});

		ui.loop();

		UiController.close();

		System.out.println("KONIEC");
		System.exit(0);
	}

	private static void loadAndStoreDefaultModels(String classpathModel, HbnSession session) throws Exception {
		try {
			DoctorDAO dao = new DoctorDAO(session.get());

			long doctorsCount = dao.count();
			if (doctorsCount > 0)
				return;

			URLConnection modelUrl = ClasspathURLStreamHandler.getURLConnection("classpath:" + classpathModel);
			DoctorsModel models = read(modelUrl, DoctorsModel.class);

			for (DoctorModel model : models.getDoctors()) {
				dao.save(model);
			}
		} catch (Exception e) {
			System.err.println("Unable to load and/or store in DB default models: " + e.toString());
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	private static <T> T read(URLConnection con, Class<?> T) throws IOException {
		try (InputStream is = con.getInputStream()) {
			String body = readInputStream(is);
			ObjectMapper mapper = new ObjectMapper();
			return (T) mapper.readValue(body, T);
		}
	}

	private static String readInputStream(InputStream is) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader((is)));
		StringBuilder sb = new StringBuilder();
		String output;
		while ((output = br.readLine()) != null) {
			sb.append(output);
		}
		return sb.toString();
	}
}

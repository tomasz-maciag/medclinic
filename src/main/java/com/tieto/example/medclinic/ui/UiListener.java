package com.tieto.example.medclinic.ui;

import com.tieto.example.medclinic.logic.Subject;

public interface UiListener {
	public void onUiCreate(Subject subject);
	public void onUiRead(Subject subject);
	public void onUiDelete(Subject subject);
}

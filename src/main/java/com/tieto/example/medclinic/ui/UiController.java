package com.tieto.example.medclinic.ui;

import java.util.Scanner;

import com.tieto.example.medclinic.logic.Subject;

public class UiController {

	private int menuLevel = 1;
	private UiListener listener;
	private Subject subject;
	private static Scanner input;

	public UiController(UiListener listener) {
		System.out.println("---------- MEDCLINIC -----------");
		this.listener = listener;
		if (input == null)
			input = new Scanner(System.in);
	}

	public void loop() {
		while (true) {
			switch (menuLevel) {
				case 1:
					System.out.println("Lekarz / Pacjent / Wyjscie");
					break;
				case 2:
					System.out.println("Dodaj / Listuj / Usun / Powrot");
					break;
			}

			char choice = input.next().toLowerCase().trim().charAt(0);

			switch (menuLevel) {
				case 1:
					switch (choice) {
						case 'l':
							subject = Subject.DOCTOR;
							menuLevel = 2;
							break;
						case 'p':
							subject = Subject.PATIENT;
							menuLevel = 2;
							break;
						case 'w':
							input.close();
							return;
					}
					break;
				case 2:
					switch (choice) {
						case 'd':		// dodaj
							listener.onUiCreate(subject);
							break;
						case 'l':		// listuj
							listener.onUiRead(subject);
							break;
						case 'u':		// usuń
							listener.onUiDelete(subject);
							break;
						case 'p':		// powrót
							menuLevel = 1;
							break;
					}
					break;
			}
		}
	}

	public static int askForInteger(String message) {
		System.out.print(message);
		return input.nextInt();
	}

	public static String askForString(String name) {
		System.out.println("Podaj " + name + ": ");
		return input.next().trim();
	}

	public static void close() {
		if (input != null) {
			input.close();
			input = null;
		}
	}
}

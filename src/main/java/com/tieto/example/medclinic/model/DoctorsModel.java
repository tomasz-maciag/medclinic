package com.tieto.example.medclinic.model;

import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DoctorsModel {

	@JsonProperty
	private Collection<DoctorModel> doctors;

	@JsonProperty
	public Collection<DoctorModel> getDoctors() {
		return doctors;
	}
	public DoctorsModel setDoctors(Collection<DoctorModel> doctors) {
		this.doctors = doctors;
		return this;
	}
}

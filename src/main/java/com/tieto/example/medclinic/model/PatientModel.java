package com.tieto.example.medclinic.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PatientModel {

	@JsonProperty
	private String name;

	@JsonProperty
	private String surname;

	@JsonProperty
	public String getName() {
		return name;
	}

	public PatientModel setName(String name) {
		this.name = name;
		return this;
	}

	@JsonProperty
	public String getSurname() {
		return surname;
	}

	public PatientModel setSurname(String surname) {
		this.surname = surname;
		return this;
	}
}

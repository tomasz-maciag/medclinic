package com.tieto.example.medclinic.model;

import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DoctorModel {

	@JsonProperty
	private String name;

	@JsonProperty
	private String surname;

	@JsonProperty
	private String title;

	@JsonProperty
	private Collection<PatientModel> patients;

	public DoctorModel setName(String name) {
		this.name = name;
		return this;
	}

	@JsonProperty
	public String getName() {
		return name;
	}

	public DoctorModel setSurname(String surname) {
		this.surname = surname;
		return this;
	}

	@JsonProperty
	public String getSurname() {
		return surname;
	}

	public DoctorModel setTitle(String title) {
		this.title = title;
		return this;
	}

	@JsonProperty
	public String getTitle() {
		return title;
	}

	@JsonProperty
	public Collection<PatientModel> getPatients() {
		return patients;
	}

	public DoctorModel setPatients(Collection<PatientModel> patients) {
		this.patients = patients;
		return this;
	}

	public void addPatient(PatientModel patient) {
		patients.add(patient);
	}
}

package com.tieto.example.medclinic.logic;

public enum Subject {
	UNDEFINED,
	DOCTOR,
	PATIENT
};

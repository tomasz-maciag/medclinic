SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

CREATE SEQUENCE doctor_id_seq;
CREATE SEQUENCE patient_id_seq MINVALUE 1000;

CREATE TABLE doctor
(
	id					INTEGER
						DEFAULT NEXTVAL('doctor_id_seq'::regclass)
						NOT NULL
						CONSTRAINT doctor_pkey PRIMARY KEY,
	title				VARCHAR(16),
	name				VARCHAR(32) NOT NULL,
	surname				VARCHAR(64) NOT NULL
);

CREATE TABLE patient
(
	id					INTEGER
						DEFAULT NEXTVAL('patient_id_seq'::regclass)
						NOT NULL
						CONSTRAINT patient_pkey PRIMARY KEY,
	doctor_id			INTEGER
						NOT NULL
						CONSTRAINT doctor_fkey
						REFERENCES doctor,
	name				VARCHAR(32) NOT NULL,
	surname				VARCHAR(64) NOT NULL
);
